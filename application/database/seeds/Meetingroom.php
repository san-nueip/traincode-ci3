<?php


class Meetingroom extends Seeder
{
  protected $table = 'meetingroom';

  // === 產生資料數量 ===
  protected $dataNumbers = 20;

  /**
   * 欄位資料
   * 1. mysql:: 直接配置為預設mysql語法
   * 2. format string  使用 Faker\Factory 產生格式化假資料
   * 3. [format , param, prefix, postfix ]
   *  使用 Faker\Factory 產生格式化假資料(帶參數)，可使用[prefix|postfix]來配置前綴或後綴字詞
   */
  protected $tableColumns = [
    'id' => 'mysql::DEFAULT',
    'label' => [
      'format' => 'bothify',
      // for pass to format()
      'pattern' => '##??'
    ],
    'location' => [
      'format' => 'numberBetween',
      'param' => [
        'max' => 6,
        'min' => 3
      ],
      'postfix' => 'F'
    ],
    'limit' => 'randomDigit',
    'bookRule' => 'text',
    'defaultUsage' => 'mysql::NULL',
  ];


  /**
   * === 轉換格式化資料產生結構 ===
   * 直接根據  tableColumns 定義產生
   */
  public function defineFields($dataTypes=[]) {
    // get fields
    $fields = $this->db->list_fields($this->table);

    $foramted = [];

    $generator = new Faker\Generator();
    $base = new Faker\Provider\Base($generator);
    foreach ($fields as $field) {

      if( array_key_exists($field, $this->tableColumns) ){

        if( preg_match('/mysql::/s', $this->tableColumns[$field]) ){
          $foramted[$field] = $this->_mysql($field);
        }elseif( is_array($this->tableColumns[$field]) ){
          $functionName = $this->tableColumns[$field]['format'];
          $params = [];
          array_push($params, $this->tableColumns[$field]['pattern']);
          $params = array_map(function($item) use ($params){
            array_push($params, $item);
          }, $this->tableColumns[$field]['param']);

          $formatString = $base->{$functionName}($params);

          $prefixString = $this->tableColumns[$field]['prefix'];
          $postfixString = $this->tableColumns[$field]['postfix'];

          $foramted[$field] = $prefixString.$formatString. $postfixString;
        }else{
          $foramted[$field] = $base->{$functionName};
        }
      // 預設使用隨機4位字串
      }else{
        $foramted[$field] = $base->lexify();
      }

    }

    return $foramted;
  }

  /**
   * === According to table schema generate fake data ===
   * Usage: $ php cli seed Meetingroom
   */
  public function run()
  {
    $this->db->truncate($this->table);

    $datas = [];
    for($i=0; $i< $this->dataNumbers; $i++){

      $generator = new Faker\Generator();
      $base = new Faker\Provider\Base($generator);
      $lorem = new Faker\Provider\Lorem($generator);

      $field = [
        'id' => 'DEFAULT',
        'label' => $base->bothify('##??'),
        'location' => $base->numberBetween($min=3, $max=6).'F',
        'limit' => $base->numerify(),
        'bookRule' => $lorem->paragraph(),
        'defaultUsage' => 'NULL',
      ];

      $this->db->insert( $this->table, $field);
      // logger
      $datas[$i] = $field;
    }
    // var_dump($datas);
    die('done!');
  }
}
