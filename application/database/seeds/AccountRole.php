<?php

use Model\Orm\AccountInfo;

class AccountRole extends Seeder
{
  protected $table = 'account_role';

  //@todo 判斷AccountInfo筆數來設定！
  protected $dataNumbers = 50;


  protected $tableColumns = [
    'id' => 'mysql::DEFAULT',
    // @todo howto build relation automaticly
    'AccountId' => '',
    'DeptId' => '',
    'password' => '',
    'role' => '',
  ];


  /**
   * === According to table schema generate fake data ===
   * Usage: $ php cli seed Meetingroom
   */
  public function run()
  {
    $this->db->db_debug = false;

    $this->db->truncate($this->table);

    $key = $this->config->item('encryption_key');
    $this->load->library('encryption');
    $this->encryption->initialize(
      array(
        'driver' => 'openssl',
        'cipher' => 'blowfish',
        'mode' => 'cbc',
        'key' => $key,
      )
    );

    // $datas = [];
    $sql = 'SELECT `id`,`account` FROM account_info WHERE 1';
    if( $accounts=$this->db->query($sql) ) {

      foreach ($accounts->result_array() as $key => $value) {

        $field = [
          'id' => 'DEFAULT',
          'AccountId' => $value['id'],
          'DeptId' => 'NULL',
          'password' => $this->encryption->encrypt($value['account']),
          'role' => '與會者',
          // 'account' => $generator->bothify('demouser###'),
        ];
        // dd($field);
        $this->db->insert($this->table, $field);
      }

    } else {
      echo 'occure error!';
      dd($this->db->error());
    }


    for ($i = 0; $i < $this->dataNumbers; $i++) {

      // logger
      $datas[$i] = $field;
    }
    // var_dump($datas);
    die('done!');
  }
}
