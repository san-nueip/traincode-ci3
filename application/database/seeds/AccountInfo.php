<?php

class AccountInfo extends Seeder
{
  protected $table = 'account_info';

  // === 產生資料數量 ===
  protected $dataNumbers = 50;

  protected $tableColumns = [
    'id' => 'mysql::DEFAULT',
    'account',
    'name',
    'brithday',
    'email',
    'comment' => 'mysql::NULL',
  ];


  /**
   * === According to table schema generate fake data ===
   * Usage: $ php cli seed Meetingroom
   */
  public function run()
  {
    $this->db->truncate($this->table);

    $datas = [];
    for ($i = 0; $i < $this->dataNumbers; $i++) {

      $generator = new Faker\Generator();
      $generator->addProvider(new Faker\Provider\Base($generator));
      $generator->addProvider(new Faker\Provider\DateTime($generator));
      $generator->addProvider(new Faker\Provider\Person($generator));
      $generator->addProvider(new Faker\Provider\Internet($generator));

      // $name =
      $field = [
        'id' => 'DEFAULT',
        'account' => $generator->bothify('demouser###'),
        'name' => $generator->name(),
        'brithday' => $generator->date(),
        'email' => $generator->companyEmail(),
        'comment' => 'NULL',
      ];
      $this->db->insert($this->table, $field);
      // logger
      $datas[$i] = $field;
    }
    // var_dump($datas);
    die('done!');
  }
}
