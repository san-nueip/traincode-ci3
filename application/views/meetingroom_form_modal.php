<div class="modal-content">


  <div class="modal-header">
    <h5 class="modal-title" id="formModalLabel"><?= $modal_title; ?></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">


    <form method="POST" action="<?= $modal_action; ?>">
      <div class="form-group">
        <label for="label">會議室名稱</label>
        <input type="text" class="form-control" id="label" name="label" value="" />
      </div>

      <div class="form-group">
        <label for="members">所在樓層</label>
        <input type="number" class="form-control" id="location" name="location" />
      </div>

      <div class="form-group">
        <label for="members">使用人數上限</label>
        <input type="number" class="form-control" id="members" name="members" />
      </div>

      <div class="form-group">
        <label for="usage">可借用途設定</label>
        <select class="form-control opt-select-insert" name="usage" id="usage" multiple>
          <option value="discuss">檢討會議</option>
          <option value="remote">遠端會議</option>
          <option value="classroom">上課教學</option>
          <option value="training">訓練課程</option>
          <option value="live">視訊直播</option>
        </select>
        <small id="emailHelp" class="form-text text-muted">可直接輸入或使用建議標籤</small>
      </div>

      <div class="form-group">
        <label for="notice">狀態</label>
        {# 啟用 #}{# 禁用 #}
      </div>

      <div class="form-group">
        <label for="bookRule">借用辦法</label>
        <textarea class="form-control" name="bookRule" id="bookRule" rows="3"></textarea>
      </div>

    </form>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-primary">儲存</button>
  </div>

</div>

<script type="text/javascript">
  function callback() {

  }
</script>