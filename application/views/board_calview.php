<?= $assets_load; ?>


<div class="p-5">
  <h2 class="mb-4">單一會議室借用檢索介面</h2>
  <div class="card">
    <div class="card-body p-0">
      <div id="calendar"></div>
    </div>
  </div>
</div>


<?=$_content_calendar; ?>

<script>
  $(function() {
    $("#add-event").submit(function() {
      alert("Submitted");
      var values = {};
      $.each($('#add-event').serializeArray(), function(i, field) {
        values[field.name] = field.value;
      });
      console.log(
        values
      );
    });
  });

  (function() {
    'use strict';
    // ------------------------------------------------------- //
    // Calendar
    // ------------------------------------------------------ //
    $(function() {
      // page is ready
      $('#calendar').fullCalendar({
        themeSystem: 'bootstrap4',
        // emphasizes business hours
        businessHours: false,
        defaultView: 'agendaWeek',
        // event dragging & resizing
        editable: true,
        // header
        header: {
          left: 'title',
          center: 'month,agendaWeek,agendaDay',
          right: 'today prev,next'
        },
        events: [{
            title: '開會',
            description: '這裡顯示借用資訊',
            start: '2020-04-05T14:00:00',
            end: '2020-04-05T20:00:00',
            className: 'fc-bg-deepskyblue',
            icon: "cog",
            allDay: false
          },
          // {
          //   title: 'Team Meeting',
          //   description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu pellentesque nibh. In nisl nulla, convallis ac nulla eget, pellentesque pellentesque magna.',
          //   start: '2020-04-23T13:00:00',
          //   end: '2020-04-23T16:00:00',
          //   className: 'fc-bg-pinkred',
          //   icon: "group",
          //   allDay: false
          // },
          // {
          //   title: 'Dinner',
          //   description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu pellentesque nibh. In nisl nulla, convallis ac nulla eget, pellentesque pellentesque magna.',
          //   start: '2020-11-15T20:00:00',
          //   end: '2020-11-15T22:30:00',
          //   className: 'fc-bg-default',
          //   icon: "cutlery",
          //   allDay: false
          // },
          // {
          //   title: 'Dentist',
          //   description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu pellentesque nibh. In nisl nulla, convallis ac nulla eget, pellentesque pellentesque magna.',
          //   start: '2020-04-29T11:30:00',
          //   end: '2020-04-29T012:30:00',
          //   className: 'fc-bg-blue',
          //   icon: "medkit",
          //   allDay: false
          // }
        ],
        eventRender: function(event, element) {
          // if (event.icon) {
          //   element.find(".fc-title").prepend("<i class='fa fa-" + event.icon + "'></i>");
          // }
        },
        dayClick: function() {
          $('#modal-view-event-add').modal();
        },
        eventClick: function(event, jsEvent, view) {
          $('.event-icon').html("<i class='fa fa-" + event.icon + "'></i>");
          $('.event-title').html(event.title);
          $('.event-body').html(event.description);
          $('.eventUrl').attr('href', event.url);
          $('#modal-view-event').modal();
        },
      })
    });
  })($);
</script>