<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
<link href="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">首頁</a></li>
    <li class="breadcrumb-item active" aria-current="page">申請借用</li>
  </ol>
</nav>


<section class="container">

  <form class="form" method="POST" action="/book/save">
    <div class="form-group">
      <label>借用者資訊</label>
      <span class="badge badge-info"> <?= $this->session->userdata('user_account')['name']; ?> </span>
      <input type="hidden" id="AccountId" name="AccountId" value=" <?= $this->session->userdata('user_account')['id']; ?> " />
    </div>

    <div class="form-group">
      <label for="mettingroom">選擇會議室 </label>
      <select class="form-control" name="MeetingroomId" id="MeetingroomId">
        <?php
        foreach ($Meetingrooms as $id => $roomlabel) {
          if (!empty($formdata) && $formdata['MeetingroomId'] == $id) {
            echo "<option value='{$id}' selected>{$roomlabel}</option>";
          } else if ($MeetingroomId && $MeetingroomId == $id) {
            echo "<option value='{$id}' selected>{$roomlabel}</option>";
          } else {
            echo "<option value='{$id}'>{$roomlabel}</option>";
          }
        }
        ?>
      </select>
    </div>

    <div class="form-group">
      <label for="startDateTime">會議開始時間</label>
      <div class="input-group">
        <input type="text" class="form-control datetimepicker-input" id="startDateTime" name="startDateTime" data-toggle="datetimepicker" data-target="#startDateTime" />
      </div>
    </div>

    <div class="form-group">
      <label for="endDateTime">會議結束時間</label>
      <div class="input-group">
        <input type="text" class="form-control datetimepicker-input" id="endDateTime" name="endDateTime" data-toggle="datetimepicker" data-target="#endDateTime" />
      </div>
    </div>

    <div class="form-group">
      <label for="usage">用途</label>
      <select class="form-control opt-select-insert" name="usage" id="usage">
        <option value="discuss">檢討會議</option>
        <option value="remote">遠端會議</option>
        <option value="classroom">上課教學</option>
        <option value="training">訓練課程</option>
        <option value="live">視訊直播</option>
      </select>

      <small id="emailHelp" class="form-text text-muted">可直接輸入或使用建議標籤</small>
    </div>

    <div class="form-group">
      <label for="members">與會人數</label>
      <input type="number" class="form-control" id="members" name="members" value="1" />
    </div>

    <div class="form-group">
      <label for="notice">注意事項</label>
      <textarea class="form-control" name="notice" id="notice" rows="3"></textarea>
    </div>

    <div class="form-group form-check">
      <input type="checkbox" class="form-check-input" id="notify" name="notify" value="1" />
      <label class="form-check-label" for="notify">審核後通知</label>
    </div>
    <button type="submit" class="btn btn-primary">送出</button>
  </form>

</section>

<script type="text/javascript">
  $(function() {

    //@todo 加入開始與結束時間連動、限制當日時段、
    $('#startDateTime, #endDateTime').datetimepicker({
      format: 'YYYY-MM-DD HH:mm:ss'
    });

    //@todo 加入select2-bootstrap4樣式
    $('.opt-select-insert').select2({
      placeholder: '選擇建議項目或直接輸入..'
    });
  });
</script>