<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">首頁</a></li>
    <li class="breadcrumb-item active" aria-current="page">使用者權限設定</li>
  </ol>
</nav>

<section id="dropzoneCard" class="card" style="display: none;">
  <form action="/manage/import" class="dropzone" enctype="multipart/form-data">
    <div class="card-body">
      <div class="fallback">
        <input type="file" name="file" />
      </div>
    </div>
    <small class="form-text text-muted">可拖曳檔案至此，或點選此區塊選擇csv檔案。</small>
  </form>

  <style>
    #dropzoneCard {
      position: relative;
    }

    #dropzoneCard small {
      position: absolute;
      top: 0;
      left: 5px;
    }
  </style>
</section>

<section class="card">
  <form id="userslistForm" method="post" action="/manage/usersDeleteSelect">
    <h3 class="card-header">
      <div class="row justify-content-start">
        <div class="col-6" data-toggle="popover" data-placement="top" data-html="true" data-title="Some tooltip text!" data-content='<pre>
[新增表單]
帳號：字母全部轉小寫才寫入資料庫
性別：顯示時為 男/女 ，資料庫儲存為 1/0
生日：顯示時格式 2019年2月15日 ，資料庫儲存格式為 2019-02-15
[匯出資料](text or excel)
完成「刪除」、「批次刪除」功能
完成「匯出」、「匯出」功能(using ci3 helper)
</pre>'>
          使用者列表
          <img src="/assets/icons/question-circle-fill.svg" alt="Icon" width="16" height="16" />
        </div>
        <div class="col-6 text-right">
          <button type="button" class="btn btn-primary opt-create">新增使用者</button>
          <button type="submit" class="btn btn-danger opt-deleteselect">刪除選擇項目</button>
          <button type="button" class="btn btn-info opt-import" data-toggle="button" aria-pressed="false" autocomplete="off">匯入清單</button>
          <button type="button" class="btn btn-info opt-export">匯出清單</button>
        </div>
      </div>
    </h3>

    <div class="card-body">

      <table id="dataTable" class="table table-bordered">
        <thead>
          <tr>
            <th>＃</th>
            <th>帳號</th>
            <th>部門</th>
            <th>身份</th>
            <th>姓名</th>
            <th>生日</th>
            <th>信箱</th>
            <th>
              操作
              <div class="btn-group-toggle btn-sm" data-toggle="buttons">
                <label class="btn btn-secondary">
                  <input type="checkbox" id="toggleAll" autocomplete="off"> 全選
                </label>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($tabledatas as $row) : ?>
            <tr>
              <td>
                <?= $row->id; ?>
              </td>
              <td><?= $row->account; ?></td>
              <td><?= $row->email; ?></td>
              <td><?= $row->role->role; ?></td>
              <td><?= $row->name; ?></td>
              <td><?= $row->brithday; ?></td>
              <td><?= $row->email; ?></td>
              <td>
                <button type="button" class="btn btn-sm btn-secondary opt-select">
                  <input type="checkbox" id="rowdel_<?= $row->id; ?>" name="pid[]" value="<?= $row->id; ?>" />選擇
                </button>
                <button type="button" class="btn btn-sm btn-primary opt-update" data-id="<?= $row->id; ?>" data-name="<?= $row->name; ?>">修改</button>
                <button type="button" class="btn btn-sm btn-danger opt-delete" data-id="<?= $row->id; ?>">刪除</button>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

    </div>
  </form>
</section>


<script type="text/javascript">
  $(function() {
    $(document).on('click', ".opt-create", function() {
      $.ajax({
        url: '/manage/usersForm',
        method: "GET",
        data: {
          title: '新增使用者表單'
        },
        dataType: "html",
        beforeSend: function() {},
        success: function(resp) {
          // console.log( resp );
          $(".modal-dialog").html(resp);
        },
        error: function(jqXHR) {},
        complete: function() {
          $('#formModal').modal('show');
          // call load page/block javascript
          callback_modal();
        }
      });
    });

    $(document).on('click', ".opt-update", function() {
      // 取得使用者編號
      var pid = $(this).data('id');
      var name = $(this).data('name');
      $.ajax({
        url: '/manage/usersForm',
        method: "GET",
        data: {
          title: '編輯' + name + '資訊',
          primaryId: pid
        },
        dataType: "html",
        beforeSend: function() {},
        success: function(resp) {
          // console.log( resp );
          $(".modal-dialog").html(resp);
        },
        error: function(jqXHR) {},
        complete: function() {
          $('#formModal').modal('show');
          // call load page/block javascript
          // callback_modal();
        }
      });
    });


    $(document).on('click', ".opt-delete", function() {
      // 取得使用者編號
      var pid = $(this).data('id');
      var name = $(this).data('name');

      if (confirm('確認要直接刪除' + name + '的帳號嗎？無法復原')) {
        $.ajax({
          url: '/manage/usersDelete',
          method: "POST",
          data: {
            primaryId: pid
          },
          dataType: "json",
          beforeSend: function() {},
          success: function(resp) {
            if (resp.code == 200) {
              window.location.reload();
            } else {
              alert(resp.errMsg);
            }
          },
          error: function(jqXHR) {},
        });
      }
    });

    $("#dataTable").DataTable({
      "dom": "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
      "language": {
        "emptyTable": "無資料...",
        "processing": "處理中...",
        "loadingRecords": "載入中...",
        "lengthMenu": "顯示 _MENU_ 項結果",
        "zeroRecords": "沒有符合的結果",
        "info": "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
        "infoEmpty": "顯示第 0 至 0 項結果，共 0 項",
        "infoFiltered": "(從 _MAX_ 項結果中過濾)",
        "infoPostFix": "",
        "search": "搜尋:",
        "paginate": {
          "first": "第一頁",
          "previous": "上一頁",
          "next": "下一頁",
          "last": "最後一頁"
        },
        "aria": {
          "sortAscending": ": 升冪排列",
          "sortDescending": ": 降冪排列"
        }
      },
      "columns": [{
          "width": '5%'
        },
        {
          "width": '12%'
        },
        {
          "width": '10%'
        },
        {
          "width": '8%'
        },
        {
          "width": '15%'
        },
        {
          "width": '10%'
        },
        {
          "width": '15%'
        },
        {
          "width": '25%'
        },
      ],
      "columnDefs": [{
          "targets": [0, 7],
          "searchable": false,
        },
        {
          "targets": [6, 7],
          "sortable": false,
        },
      ]
    });

    $("#toggleAll").click(function() {
      var checkBoxes = $("input[name=pid\\[\\]]");
      checkBoxes.prop("checked", !checkBoxes.prop("checked"));
    });
    $(document).on('click', "button.opt-select", function(e) {
      var item = $(this).find(":checkbox");
      item.prop("checked", !item.prop("checked"));
    });

    $(".opt-import").click(function() {
      $("#dropzoneCard").fadeToggle("slow", "linear");
    });

    $("#dropzoneCard").dropzone({
      url: "/manage/import",
      acceptedFiles: ".csv,.xls",
      maxFilesize: 10, // MB
      accept: function(file, done) {},
      init: function() {
        this.on("success", function(file, done) {
          console.log(done);
          alert('檔案已上傳！');
        })
        this.on("queuecomplete", function() {
          window.location.reload();
        })
      }
    });

    $(".opt-export").click(function() {
      // trigger file download
      window.location.href = '/manage/usersExport'
    });
  });
</script>