<?= $assets_load; ?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">首頁</a></li>
    <li class="breadcrumb-item active" aria-current="page">借用看板</li>
  </ol>
</nav>



<section class="card">
  <h3 class="card-header">(行事曆模式)會議室借用資訊</h3>
  <div class="card-body row">

    <?php foreach ($meetingrooms as $id => $label) : ?>
      <div id="meetingroom_<?= $id; ?>" class="col-6" style="margin:5px auto;">
        <div class="card">
          <h3 class="card-header">
            <div class="row">
              <div class="col-12"><?= $label; ?></div>
            </div>
          </h3>
          <div class="card-body">
            <div class="calendar-block"></div>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</section>


<?= $_content_calendar; ?>

<script>
  $(function() {
    $("#add-event").submit(function() {
      alert("Submitted");
      var values = {};
      $.each($('#add-event').serializeArray(), function(i, field) {
        values[field.name] = field.value;
      });
      console.log(
        values
      );
    });
  });

  (function() {
    'use strict';
    // ------------------------------------------------------- //
    // Calendar
    // ------------------------------------------------------ //
    $(function() {
      // page is ready
      $('.calendar-block').fullCalendar({
        themeSystem: 'bootstrap4',
        // emphasizes business hours
        businessHours: false,
        defaultView: 'agendaWeek',
        // event dragging & resizing
        editable: false,
        header: {
          left: 'month,agendaWeek',
          right: 'today prev,next'
        },
        // @todo variable from backend
        events: [],
        eventRender: function(event, element) {
          // if (event.icon) {
          //   element.find(".fc-title").prepend("<i class='fa fa-" + event.icon + "'></i>");
          // }
        },
        dayClick: function() {
          $('#modal-view-event-add').modal();
        },
        eventClick: function(event, jsEvent, view) {
          $('.event-icon').html("<i class='fa fa-" + event.icon + "'></i>");
          $('.event-title').html(event.title);
          $('.event-body').html(event.description);
          $('.eventUrl').attr('href', event.url);
          $('#modal-view-event').modal();
        },
      })
    });
  })($);
</script>