<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/cisystem/">首頁</a></li>
    <li class="breadcrumb-item active" aria-current="page">會議室管理</li>
  </ol>
</nav>

<section class="card">
  <div class="card-header"></div>
  <div class="card-body">
    <pre>
    {# 2樓 Ext. 608 #}
    {# 3樓 Ext. 701 #}

    {# 4樓 Ext. 125 #}

    {# 5樓 Ext. 601 #}
    {# 5樓 Ext. 602 #}

    {# 6樓 Ext. 751 #}
    </pre>
  </div>
</section>

<section class="card">
  <h3 class="card-header">dataTables列表資訊</h3>
  <div class="card-body">

    <div class="row">
      <span>操作功能：</span>
      <button class="btn btn-primary opt-create">新增</button>
      <button class="btn btn-info opt-import">匯入</button>
      <button class="btn btn-info opt-export">匯出</button>
    </div>

    <table id="dataTable" class="table table-bordered">
      <thead>
        <tr>
          <th>＃</th>
          <th>會議室名稱</th>
          <th>位置</th>
          <th>上限人數</th>
          <th>狀態</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($tabledatas as $row) : ?>
          <tr>
            <td><?= $row['id']; ?></td>
            <td><?= $row['label']; ?></td>
            <td><?= $row['location']; ?>樓</td>
            <td><?= $row['limit']; ?>/人次</td>
            <td>
              <?= $row['id']; ?>
            </td>
            <td>
              <button class="btn btn-sm btn-primary opt-update" data-toggle="modal" data-target="#formModal" data-id="<?= $row['id']; ?>">修改</button>

              <button class="btn btn-sm btn-warning opt-disable" data-id="<?= $row['id']; ?>">啟用</button>

              <button class="btn btn-sm btn-danger opt-delete" data-id="<?= $row['id']; ?>">移除</button>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>

  </div>
</section>


<script type="text/javascript">
  $(function() {
    $(".opt-create").click(function() {
      $.ajax({
        url: '/meetingroom/form',
        // data: $("formID").serialize(),
        method: "GET",
        dataType: "html",
        beforeSend: function() {},
        success: function(resp) {
          // console.log( resp );
          $(".modal-dialog").html(resp);
        },
        error: function(jqXHR) {},
        complete: function() {
          $('#formModal').modal('show');
        }
      });
    });

    $("#dataTable").DataTable({});
  });
</script>