<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">首頁</a></li>
    <li class="breadcrumb-item active" aria-current="page">借用紀錄</li>
  </ol>
</nav>

<section class="card">
  <h3 class="card-header">會議室當日借用紀錄</h3>

  <div class="card-body" style="min-height: 600px;">

    <div class="d-md-flex flex-row flex-wrap justify-content-around">

      <?php foreach ($meetingrooms as $id => $label) : ?>
        <div id="meetingroom_<?= $id; ?>" class="col-5" style="margin:20px auto;">
          <div class="card">
            <h3 class="card-header">
              <div class="row">
                <div class="col-8"><?= $label; ?></div>
                <div class="col-4 text-right">
                  <a class="btn btn-sm btn-primary" href="/book/form?MeetingroomId=<?= $id; ?>">我要申請借用</a>
                </div>
              </div>
            </h3>
            <div class="card-body">

              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">時段</th>
                    <th scope="col">借用者</th>
                    <th scope="col">用途</th>
                  </tr>
                </thead>
                <tbody>
                <?php if( count($bookrecords) && array_key_exists($label,$bookrecords) ):?>
                <?php foreach($bookrecords[$label] as $record):?>
                  <tr>
                    <th scope="row"><?=$record['id'];?></th>
                    <td><?=$record['time'];?></td>
                    <td><?=$record['account'];?></td>
                    <td>
                      <span class="badge"><?=$record['usage'];?></span>
                    </td>
                  </tr>
                <?php endforeach; ?>
                <?php else: ?>
                  <tr>
                    <th scope="row">1</th>
                    <td>10:00~12:00</td>
                    <td>Otto</td>
                    <td>
                      <span class="badge">教育訓練</span>

                    </td>
                  </tr>
                  <tr>
                    <th scope="row">2</th>
                    <td>13:00~14:00</td>
                    <td>Thornton</td>
                    <td>
                      <span class="badge">面試</span>
                    </td>
                  </tr>
                <?php endif; ?>

                </tbody>
              </table>

            </div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>

  </div>
</section>