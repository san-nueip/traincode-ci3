<div class="modal-content">
  <form id="usersForm" method="POST" action="<?= $modal_action; ?>">
    <?php
    if (!empty($formdata)) {
      echo "<input type='hidden' name='id' value='{$formdata['id']}' />";
    }
    ?>

    <div class="modal-header">
      <h5 class="modal-title" id="formModalLabel"><?= $modal_title; ?></h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <div class="form-group">
        <label for="account">帳號</label>
        <input type="text" class="form-control" id="account" name="account" required value="<?= (!empty($formdata)) ? $formdata['account'] : ''; ?>" />
      </div>

      <div class="form-group">
        <label for="name">姓名</label>
        <input type="text" class="form-control" id="name" name="name" required value="<?= (!empty($formdata)) ? $formdata['name'] : ''; ?>" />
      </div>

      <div class="form-group">
        <label for="sex">性別</label>
          <?php
          foreach ($sex as $key => $value) {
            if (!empty($formdata)) {
              if ($formdata['sex'] == $key) {
                print("<div class='form-check form-check-inline'>
                        <input class='form-check-input' type='radio' id='sex_{$key}' name='sex' value='{$key}' checked>
                        <label class='form-check-label' for='sex_{$key}'>{$value}</label>
                      </div>");
              } else {
                print("<div class='form-check form-check-inline'>
                        <input class='form-check-input' type='radio' id='sex_{$key}' name='sex' value='{$key}'>
                        <label class='form-check-label' for='sex_{$key}'>{$value}</label>
                      </div>");
              }
            } else {
              print("<div class='form-check form-check-inline'>
                        <input class='form-check-input' type='radio' id='sex_{$key}' name='sex' value='{$key}'>
                        <label class='form-check-label' for='sex_{$key}'>{$value}</label>
                      </div>");
            }
          }
          ?>
      </div>

      <div class="form-group">
        <label for="Dept">設定部門</label>
        <select id="Dept" name="Dept" class="form-control">
          <?php foreach ($depts as $dept) : ?>

            <option value="<?= $dept['d_id']; ?>"><?= "({$dept['d_code']}){$dept['d_name']}"; ?></option>

          <?php endforeach; ?>
        </select>
      </div>

      <div class="form-group">
        <label for="role">設定身份</label>
        <select id="role" name="role" class="form-control">
          <?php
          foreach ($roles as $role) {
            if (!empty($formdata)) {
              if ($formdata['role']['role'] == $role) {
                echo "<option value='{$role}' selected>{$role}</option>";
              } else {
                echo "<option value='{$role}'>{$role}</option>";
              }
            } else {
              echo "<option value='{$role}'>{$role}</option>";
            }
          }
          ?>
        </select>
      </div>

      <div class="form-group">
        <label for="brithday">生日</label>
        <input type="date" class="form-control" id="brithday" name="brithday" value="<?= (!empty($formdata)) ? $formdata['brithday'] : ''; ?>" />
      </div>

      <div class="form-group">
        <label for="email">信箱</label>
        <input type="email" class="form-control" id="email" name="email" value="<?= (!empty($formdata)) ? $formdata['email'] : ''; ?>" />
      </div>

      <div class="form-group">
        <label for="comment">備註</label>
        <textarea class="form-control" name="comment" id="comment" rows="3"><?= (!empty($formdata)) ? $formdata['comment'] : ''; ?></textarea>
      </div>


    </div>
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary">儲存</button>
    </div>

  </form>
</div>

<script type="text/javascript">
  function callback_modal() {
    // alert('called from callback');
  }
</script>