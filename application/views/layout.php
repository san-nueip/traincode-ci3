<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>會議室借用系統-展示程式</title>

  <link rel="stylesheet" type="text/css" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="/assets/dropzone/dist/dropzone.css">

  <script type="text/javascript" src="/node_modules/jquery/dist/jquery.min.js"></script>
  <script type="text/javascript" src="/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script type="text/javascript" src="/node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script type="text/javascript" src="/assets/dropzone/dist/dropzone.js"></script>
</head>

<body>

  <div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="/book">會議室借用系統</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- 目前只是訓練題，求快未配置所在頁面(active)機制 -->
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="/board">借用看板 <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="/book">借用會議室</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              其他操作
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <span>借用管理</span>
              <a class="dropdown-item" href="/book/form">借用申請</a>
              <a class="dropdown-item" href="/book/list">借用紀錄</a>
              <span class="sr-only">{# 若為管理者，增加選單：借用審核 #}</span>
              <div class="dropdown-divider"></div>
              <span>會議室</span>
              <a class="dropdown-item" href="/meetingroom">設定會議室</a>
              <div class="dropdown-divider"></div>
              <span>系統管理</span>
              <a class="dropdown-item" href="/manage/users">使用者身份</a>
              <a class="dropdown-item" href="/manage/settings">偏好設定</a>
            </div>
          </li>
        </ul>
      </div>
      <div class="">
        <img src="/assets/icons/people-fill.svg" alt="Icon" width="32" height="32" />
        <?=$this->session->userdata('user_account')['account'];?>
        <a href="/manage/logout" class="btn btn-primary">登出</a>
      </div>
    </nav>

    <?php if ($this->session->flashdata('errMsg')) { ?>
      <div class="alert alert-danger">
        <strong>錯誤 <?= $this->session->flashdata('errCode'); ?></strong>
        <span><?= $this->session->flashdata('errMsg'); ?></span>
      </div>
    <?php } ?>
    <?php if ($this->session->flashdata('msg')) : ?>
      <div class="alert alert-success">
        <span><?= $this->session->flashdata('msg'); ?></span>
      </div>
    <?php endif; ?>

    <section class="">
      <?= $content; ?>
    </section>

  </div>


  <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    </div>
  </div>

  <script type="text/javascript">
    $(function() {
      $('[data-toggle="tooltip"]').tooltip();
      $('[data-toggle="popover"]').popover();
    });
  </script>

</body>

</html>