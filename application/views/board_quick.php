<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">首頁</a></li>
    <li class="breadcrumb-item active" aria-current="page">借用看板</li>
  </ol>
</nav>

<section class="card">
  <h3 class="card-header">顯示各會議室借用資訊(快速模式｜group-list列表)</h3>
  <div class="card-body">

    <pre>
    {# RWD配置splitView #}
    [Bootstrap Snippet Team Design Section with Pure CSS Effect using HTML CSS Bootstrap](https://bootsnipp.com/snippets/qr9bx)

    {# 2樓 Ext. 608 #}

    {# 3樓 Ext. 701 #}

    {# 4樓 Ext. 125 #}

    {# 5樓 Ext. 601 #}
    {# 5樓 Ext. 602 #}

    {# 6樓 Ext. 751 #}
  </pre>

  </div>
</section>