<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>會議室借用系統-展示程式</title>

  <link rel="stylesheet" type="text/css" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css">


  <script type="text/javascript" src="/node_modules/jquery/dist/jquery.min.js"></script>
  <script type="text/javascript" src="/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/node_modules/datatables.net/js/jquery.dataTables.min.js">
  <script type="text/javascript" src="/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

  <style>
    .form-signin {
      width: 100%;
      max-width: 330px;
      padding: 15px;
      margin: auto;
    }
  </style>
</head>

<body>

  <div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">會議室借用系統</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

    </nav>

    <?php if ($this->session->flashdata('errMsg')) { ?>
      <div class="alert alert-danger">
        <strong>錯誤 <?= $this->session->flashdata('errCode'); ?></strong>
        <span><?= $this->session->flashdata('errMsg'); ?></span>
      </div>
    <?php } ?>

    <section class="text-center">

      <form class="form-signin" method="POST" action="">
        <img class="mb-4" src="/assets/icons/bootstrap.svg" alt="會議室借用系統" width="72" height="72" />
        <h1 class="h3 mb-3 font-weight-normal">請登入</h1>
        <label for="account" class="sr-only">使用者帳號</label>
        <input type="text" id="account" name="account" class="form-control" placeholder="登入帳號" required autofocus />
        <br />
        <label for="password" class="sr-only">Password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required />
        <div class="checkbox mb-3">
          <label>
            <input type="checkbox" value="remember-me"> 記住我
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">登入</button>
        <p class="mt-5 mb-3 text-muted">© 2020 Power by San.Huang </p>
      </form>

    </section>

  </div>

</body>

<script type="text/javascript">
  $(function() {});
</script>

</html>