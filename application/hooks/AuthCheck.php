<?php
class AuthCheck
{
  function __construct()
  {
    // get instance from CI_Controller and instance immediately if not instanced.
    $this->CI = &get_instance();
    if ($this->CI === null) {
      new CI_Controller();
      $this->CI = &get_instance();
    }
  }

  /**
   * 檢查sessin user是否存在，若否導回manage/login
   *
   * @return void
   */
  public function ifLogin()
  {
    // check path now if not /manage/(login|logout)
    if( !preg_match("/manage\/(login|logout)/ms", $this->CI->uri->uri_string) ){
      // check if session exist or redirect to /manage/login
      if( !count($this->CI->session->userdata('user_account')) ){
        redirect('http://'.$_SERVER['HTTP_HOST'].'/manage/login');
      }
    }

  }
}
