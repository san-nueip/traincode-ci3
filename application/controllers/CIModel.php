<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Model\AccountInfo;
use Model\DeptInfo;

/**
 * @see [[CodeIgniter 3] 資料庫的使用方法整理2/2 @新精讚](http://n.sfs.tw/content/index/10174)
 *
 * 1. 跳脫查詢
 * 	$this->db->escape()：在指定欄位使用
 * 	$this->db->escape_like_str()：此函數可以用在字串將使用於 SQL Like 語法當中 % _
 * 2. 封裝查詢
 * prepare statement?
 *
 * 3. Database Utilities
 *  $this->dbutil;
 *  Export a Query Result as a CSV File
 *  Export a Query Result as an XML Document
 */
class CIModel extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    // 預設關閉內建錯誤訊息輸出模式，自行處理例外錯誤發生時邏輯。
    $this->db->db_debug = false;
  }


  /*
   * === PHP-CRUD 新增部分 ===
   * 實作以pure sql mode對account_info新增單筆紀錄
   */
  public function create()
  {
    // dd($this->db);
    $sql = 'INSERT INTO `account_info` (
              `id`,
              `account`,
              `name`,
              `brithday`,
              `email`,
              `comment`
            ) VALUES (
              DEFAULT,
              "user1",
              "SanHuang",
              "1983-04-05",
              "san\.huang@staff.nueip.com",
              "insert by pure sql"
            )';

    // @done CI3如何取得db例外並處理
    // $this->db->db_debug = FALSE; //disable debugging for queries

    if ($this->db->query($sql)) {
      echo "Success insert data!";
    } else {
      echo 'occure error!';
      dd($this->db->error());
    }
    // dd($res);
  }


  /**
   * === PHP-CRUD 新增部分 ===
   * 實作以query builder mode對account_info新增單筆紀錄
   */
  public function createByBuilder()
  {
    $data =  [
      'id' => 'DEFAULT',
      'account' => 'p0065',
      'name' => 'San.Huang',
      'brithday' => '19830405',
      'email' => 'personalwork@me.com',
      'comment' => 'insert by inert()'
    ];

    if (!($res = $this->db->insert('account_info', $data))) {
      dd($this->db->error());
    } else {
      echo "Success insert data!";
    }
  }


  /**
   * === PHP-CRUD 更新部分 ===
   * 實作以
   * 1. pure sql mode
   * 2. query builder mode
   * 實現對account_info更新紀錄
   */
  public function update()
  {
    // dd($this->db);
    $sql = 'UPDATE account_info
            SET `account`="account-updated"
            WHERE `id`=1';
    if ($this->db->query($sql)) {
      echo "Success update data!";
    } else {
      echo 'occure error!';
      dd($this->db->error());
    }

    // or
    // $res = $this->db->where('id', 1)->update([
    //   'account' => 'account-updated'
    // ]);
    // if (!$res) {
    //   echo 'occure error!';
    //   dd($this->db->error());
    // }
  }


  /**
   * === PHP-CRUD 更新部分 ===
   * 實作以pure sql mode實現對account_info刪除單筆紀錄
   */
  public function delete()
  {
    $sql = 'DELETE FROM `account_info` WHERE `id`=1';
    if ($this->db->query($sql)) {
      echo "Success delete data!";
    } else {
      echo 'occure error!';
      dd($this->db->error());
    }
  }


  /**
   * === 複製PHP-CRUD Dept_info範本 ===
   */
  public function Dept_info()
  {
    // dd($this->session);

    // 載入部門資料庫並建立簡化別名deptInfo
    $this->load->model('Dept_info_model', 'deptInfo');

    echo "<pre>";

    /**
     * ========== 範例-新增 ==========
     */
    // 新增一筆資料 - 使用session記錄計數
    if (!isset($_SESSION['count'])) {
      $_SESSION['count'] = 0;
    }
    $count = ++$_SESSION['count'];

    // 建構新增範例資料
    $data = [
      'd_code' => 'd' . str_pad($count, 4, '0', STR_PAD_LEFT),
      'd_name' => '部門1',
      'd_level' => '部',
      'date_start' => '2020-01-01',
      'remark' => '部門' . $count,
    ];

    // 使用deptInfo中的post函式新增資料
    if (!$d_id = $this->deptInfo->post($data)) {
      dd($data, $this->db->error());
    }

    echo "新增：" . $d_id;
    echo "\n";
    echo "\n";



    /**
     * ========== 範例-讀取 ==========
     */
    // 讀取剛才新增的資料
    $data = $this->deptInfo->get($d_id);
    echo "讀取：";
    var_export($data);
    echo "\n";



    /**
     * ========== 範例-修改 =========
     */
    // 建構修改範例資料 - 修改剛才新增的資料
    $data = [
      'd_id' => $d_id,
      'd_name' => '部門' . mt_rand(0000, 9999),
      'd_level' => '組',
    ];

    // 使用deptInfo中的put函式修改資料
    $d_id = $this->deptInfo->put($data);
    // 讀回修改的資料
    $data = $this->deptInfo->get($d_id, 'd_id,d_name,d_level');
    echo "修改後：";
    var_export($data);
    echo "\n";



    /**
     * ========== 範例-刪除 ==========
     */
    // 使用deptInfo中的put函式修改資料 - 軟刪
    $this->deptInfo->delete($d_id);
    // 使用deptInfo中的put函式修改資料 - 強刪
    // $this->deptInfo->delete($d_id, true);

    // 讀回刪除的資料
    $data = $this->deptInfo->get($d_id);
    echo "刪除後：";
    var_export($data);
    echo "\n";
    echo "請至資料庫中查看d_id=" . $d_id . "資料狀態";
    echo "\n";
    echo "\n";


    /**
     * ========== 範例-取得資料-從查詢條件 ==========
     * 請先新增好一些資料再來查詢
     */
    $where = [
      'd_id' => ['31', '32'],
      'd_level' => '部',
    ];
    $data = $this->deptInfo->getBy($where);
    var_export($data);
  }
}
