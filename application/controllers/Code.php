<?php

use phpDocumentor\Reflection\Types\Integer;
use Prophecy\Exception\Doubler\ReturnByReferenceException;

defined('BASEPATH') OR exit('No direct script access allowed');

class Code extends CI_Controller {

	public function index()
	{ echo 'Hello Code Action!'; }


	/**
	 * 九九乘法表
	 * 變數 乘積／被乘績 （結果）
	 * 排版方式
	 * 	console print
	 * 	html output
	 * @return void
	 */
	public function nntable()
	{
		// === 使用表格輸出版本 ===
		echo "<table>";
		// 乘積
		for ($x = 0; $x <= 9; $x++) {
			echo '<tr >';
			if ($x != 0) {
				echo '<td>' . str_pad($x, 4, ' ', STR_PAD_BOTH) . '</td>';
			}
			// 被乘積
			for ($y = 0; $y <= 9; $y++) {
				if ($x == 0) {
					if ($y == 0) {
						echo "<td>&nbsp;</td>";
					} else {
						echo "<td align='right'>" . $y . "</td>";
					}
				} elseif ($y != 0) {
					echo "<td align='right'>" . $x * $y . "</td>";
				}
			}
			echo "</tr>";
		}
		echo '</table>';
	}


	protected function _bubble_sort(Array $ints) : Array
	{
		$len = count($ints);
		// 對於一個長度為$len的陣列，共需比對($len-1)次
		for ($i=$len-1; $i>0; $i--) {
				// 第一次需要筆對($len-1)次
				for ( $j=0; $j<$i; $j++) {
						if ($ints[$j]>$ints[$j+1]){
								// 置換
								$ints[$j] = $ints[$j+1] + $ints[$j];
								$ints[$j+1] = $ints[$j] - $ints[$j+1];
								$ints[$j] = $ints[$j] - $ints[$j+1];
						}
				}
		}
		return $ints;
	}

	/**
	 * === 氣泡排序法 ===
	 * 功能群組註解詳細說明
	 */
	public function bubbleSort()
	{
		$bArray = [2, 5, 66, 1, 63, 29, 45];
		dd('排序前: ', $bArray, '排序後: ', $this->_bubble_sort($bArray) ) ;
	}


	protected function _find_smallest($target_array = [])
	{
		// 先以傳入陣列第一個位置數值作為最小參考值
		$small_list = $target_array[0];
		// 先以傳入陣列第一個位置索引作為最小參考索引
		$small_index = 0;
		foreach ($target_array as $index => $value) {
			// 當迴圈數值小於參考值，以該值資訊取代最小值與最小值索引
			if ($target_array[$index] < $small_list) {
				$small_list = $target_array[$index];
				$small_index = $index;
			}
		}
		// 回傳最小值索引
		return $small_index;
	}


	protected function select_sort($origin_array = [])
	{
		// 建立排序後陣列
		$new_array = [];
		foreach ($origin_array as $key => $value) {
			// 將以排序後的索引/值移出進行的排序陣列
			if (isset($small_index)) {
				// 移除找到的最小值
				unset($origin_array[$small_index]);
				// 去除該索引位置重新索引產生待排序
				$origin_array = array_values($origin_array);
			}

			// 依照前述取陣列最小值函式取得該值索引
			$small_index = $this->_find_smallest($origin_array);
			// 根據最小索引值將數值配置新的陣列
			$new_array[$key] = $origin_array[$small_index];
		}

		return $new_array;
	}


	/**
	 * === 選擇排序法 ===
	 * 功能群組註解詳細說明
	 */
	public function selectSort()
	{
		$sArray = [4, 14, 133, 44, 12, 1];
		// result: [1,4,12,14,44,133]
		dd('排序前: ', $sArray, '排序後: ', $this->select_sort($sArray));
	}


	/**
	 * 以迴圈陣列方式處理費氏數列
	 *
	 * @param [type] $n (X_n)
	 * @param integer $first （X_n-1）
	 * @param integer $second （X_n-2）
	 * @return void
	 */
	protected function _formula_fibonacci($n, $first = 0, $second = 1)
	{
		// 利用Array儲存$first / $second數值
		$fib = [$first, $second];
		for ($i = 1; $i < $n; $i++) {
			// 儲存前一數值、前二數值計算結果
			$fib[] = $fib[$i] + $fib[$i - 1];
		}

		// 將每個索引值加總
		return $fib;
	}


	/**
	 * 以遞迴方式處理費氏數列
	 *
	 * @param [type] $n (X_n)
	 * @return intger （X_n-1）+（X_n-2）
	 */
	protected function _recurive_fibonacci($n): int
	{
		// 基本情況：根據規則公式，數值小於2相當於數值本身
		if ($n < 2) {
			return $n;
		}
		// 歸納基本情況：公式為目前數值等於前一數值與前二數值相加
		return intval($this->_recurive_fibonacci($n - 1) + $this->_recurive_fibonacci($n - 2));
	}


	/**
	 * === 斐波那契數列（費氏數列） ===
	 * 公式：X_n = X_n-1 + X_n-2
	 */
	public function fibonacci()
	{
		$n = 30;

		$fArray = $this->_formula_fibonacci($n);
		echo "以迴圈陣列方式，顯示數值({$n})的費氏數列: <BR/>";
		foreach ($fArray as $key => $value) {
			echo $value;
			if ($key != (count($fArray) - 1)) {
				echo ', ';
			}
		}
		echo '<BR/><BR/>';

		echo "以遞迴方式，處理第({$n})數值的費氏數值: " . $this->_recurive_fibonacci($n);
	}


	protected function _insertion_sort(Array $unsortedArray): Array
	{
			$len = count($unsortedArray);
			$resultArray = Array();

			for ($i=0; $i<$len; $i++){
				// 建立已排序部分
				if ($i==0){
						$resultArray[0] = $unsortedArray[$i];
						continue;
				}
				// 取出未排序片段
				$currentArray = $unsortedArray[$i];
				// 進行n-1次迴圈進行排序
				for ($j=0; $j<$i; $j++){
					// 判斷當目前數值小於已排序部分當前索引，則進行置換插入已排序當前位置
					if ($currentArray < $resultArray[$j]){
							$temp = $resultArray[$j];
							$resultArray[$j] = $currentArray;
							$currentArray = $temp;
					}
					// 迴圈最後時(n-1)直接將數值設定在當前位置
					if ($j==$i-1){
							$resultArray[$i] = $currentArray;
					}
				}
			}
			return $resultArray;
	}

	/**
	 * === 插入排序法 ===
	 * 把n個值陣列視為已排序和未排序兩個部分的結構
	 * 開始時已排序部分只有1個數值，而未排序部分則為n-1個
	 * 排序方式為每次迴圈從未排序部分取出第一個元素，根據比較邏輯插入到已排序部分適當位置
	 * 如此重複直到經過未排序部分(n-1次)結束
	 */
	public function insertSort()
	{
		$iArray = [2, 5, 66, 1, 63, 29, 45];
		dd('排序前: ', $iArray, '排序後: ', $this->_insertion_sort($iArray));
	}


	/**
	 * === 快速排序法 ===
	 * 對於排序陣列內取一個基準比較數值，以此數值將陣列分割前後部分
	 * 排序過程中必須確保前段部分都比後段部分來的小
	 * （實際上並未真實分割陣列變數，而是透過指定索引位置以及遞迴進行排序）
	 */
	public function quickSort(): int{
		// 為了看出排序前後，故多設定未排序前變數
		$uqSort = $qSort = [3,335,12,22,55,11,78];
		$this->_quick_sort(0, count($qSort) - 1, $qSort);
		dd('排序前: ', $uqSort, '排序後: ', $qSort);
	}


	/**
	 * 使用傳址方式確保快速排序過程作用於原始陣列
	 *
	 * @param integer $begin 前段部分
	 * @param integer $end 後段部分
	 * @param Array $ints 待排序的陣列
	 * @return void
	 */
	public function _quick_sort(int $begin, int $end, Array &$ints)
	{
		// 從一開始的數值進行比對
		$intsBegin = $begin;
		// 從結尾的數值進行比對
		$intsEnd = $end;
		// 把第一個索引數值作為比對指標
		$base = $ints[$begin];
		// 進行排序過程索引變動變數
		$finalIndex = $begin;

		// false：以前段排序；true：以後段排序
		$flag = false;

		// 後段數值大於前段數值
		while($begin < $end){
			if (!$flag){
				// 後段部分：往前比對大於當時索引數值時變更數值索引
				if($base > $ints[$end]){
					$ints[$begin] = $ints[$end];
					$finalIndex = $end;
					$flag = true;
					continue;
				}
				$end--;
			} else {
				// 前段部分：往後比對小於當時索引數值時變更數值索引
				if($base < $ints[$begin]){
					$ints[$end] = $ints[$begin];
					$finalIndex = $begin;
					$flag = false;
					continue;
				}
				$begin++;
			}
		}

		$ints[$finalIndex] = $base;

		// 當下片段屬於前段排序
		if(($finalIndex-$intsBegin)>1){
			$this->_quick_sort($intsBegin,$finalIndex-1,$ints);
		}

		// 當下片段屬於後段排序
		if(($intsEnd-$finalIndex)>1){
			$this->_quick_sort($finalIndex+1,$intsEnd,$ints);
		}
	}
}
