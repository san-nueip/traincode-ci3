<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * === 資訊看板 ===
 * @author San.Huang<san.huang@staff.nueip.com>
 *
 */
class Board extends CI_Controller
{

  public function index()
  {
    $assets = <<<HTML
<link href='/node_modules/fullcalendar/dist/fullcalendar.min.css' rel='stylesheet' />
<link href='//maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css' rel='stylesheet' />
<script src='//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js'></script>
<script src='/node_modules/fullcalendar/dist/fullcalendar.min.js'></script>
<script src='/node_modules/fullcalendar/dist/locale/zh-tw.js'></script>
HTML;

    $meetingrooms = [];
    \Model\Orm\Meetingroom::all()->filter(function ($row) use (&$meetingrooms) {
      $meetingrooms[$row->id] = $row->label;
    });

    $data = [
      'meetingrooms' => $meetingrooms,
      'assets_load' => $assets,
      '_content_calendar' => $this->load->view('block_calview', null, TRUE)
    ];
    $view['content'] = $this->load->view('board_index', $data, TRUE);
    $this->load->view('layout', $view);
  }


  /**
   * === 借用資訊看板標準模式 ===
   * 以大尺寸畫面考量的介面模式
   *
   * 1. 判斷phone則提示導向 quick mode
   */
  public function normal($params = [])
  {
    // 抓取參數 $device from js param


    $view['content'] = $this->load->view('board_normal', NULL, TRUE);
    $this->load->view('layout', $view);
  }


  /**
   * === 借用資訊看板快速模式 ===
   * 以小尺寸畫面考量的介面模式，觸控點選、兩層操作（盡可能使用popover呈現資訊、詳細資訊操作查看詳細、返回）
   *
   * 1. 判斷非phone則提示導向 normal mode
   */
  public function quick($params = [])
  {
    // 抓取參數 $device from js param

    // 若非 phone 導向 normal

    // 實現兩層操作模式

    // $view['content'] = $this->load->view('board_quick', NULL, TRUE);
    $this->load->view('board_quick', $view);
  }


  /**
   * === 行事曆模式 ===
   *
   * @return void
   */
  public function calview()
  {
    $assets = <<<HTML
<link href='/node_modules/fullcalendar/dist/fullcalendar.min.css' rel='stylesheet' />
<link href='//maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css' rel='stylesheet' />
<script src='//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js'></script>
<script src='/node_modules/fullcalendar/dist/fullcalendar.min.js'></script>
<script src='/node_modules/fullcalendar/dist/locale/zh-tw.js'></script>
HTML;

    $data = [
      'assets_load' => $assets,
      '_content_calendar' => $this->load->view('block_calview', null, TRUE)
    ];

    $view['content'] = $this->load->view('board_calview', $data, TRUE);
    $this->load->view('layout', $view);
  }


  /**
   * === 處理查詢後端請求回應 ===
   * 1. 支援模糊搜尋：日期、文字格式判斷
   */
  public function ajaxSearch()
  {
    // get request and chech by php origional function

    // response format
  }
}
