<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * === 載入ORM class使用方式 ===
 * @author San.Huang<san.huang@staff.nueip.com>
 *
 * 	use Model\Orm\AccountInfo;
 * 	use Model\Orm\AccountRole;
 * 	use Model\Orm\DeptInfo;
 * 	use Model\Orm\BookRecord;
 * 	use Model\Orm\BookStatecode;
 * 	use Model\Orm\Meetingroom;
 * 	use Model\Orm\MeetingroomUsage;
 *	use Model\Orm\Settings;
 */
class Book extends CI_Controller
{

  public function index()
  {
    $mapping = [
      "discuss" => '檢討會議',
      "remote" => '遠端會議',
      "classroom" => '上課教學',
      "training" => '訓練課程',
      "live" => '視訊直播',
    ];

    $bookrecords = [];
    \Model\Orm\BookRecord::all()->filter(function ($row) use (&$bookrecords, $mapping) {
      // $bookrecords[$row->id] = $row->toArray();
      // $bookrecords[$row->id]['AccountInfo'] = $row->AccountInfo->toArray();
      // $bookrecords[$row->id]['Meetingroom'] = $row->Meetingroom->toArray();

      // #
      // 時段
      // 借用者
      // 用途
      $st = explode(' ', $row['startDateTime']);
      $et = explode(' ', $row['endDateTime']);
      $data = [
        'id' => $row->id,
        'time' => $st[1] . '~' . $et[1],
        'account' => $row->AccountInfo->name,
        'usage' => $mapping[$row->usage]
      ];
      if (array_key_exists($row->Meetingroom->label, $bookrecords)) {
        array_push($bookrecords[$row->Meetingroom->label], $data);
      } else {
        $bookrecords[$row->Meetingroom->label][] = $data;
      }
    });
    // dd($bookrecords);

    $meetingrooms = [];
    \Model\Orm\Meetingroom::all()->filter(function ($row) use (&$meetingrooms) {
      $meetingrooms[$row->id] = $row->label;
    });


    // @todo [Question] CI3有無preDispatch / postDispatch機制？
    $datas = [
      'meetingrooms' => $meetingrooms,
      'bookrecords' => $bookrecords
    ];

    $view['content'] = $this->load->view('book_index', $datas, TRUE);
    $this->load->view('layout', $view);
  }


  /**
   * === 借用表單 ===
   * form UI
   * 1. 配置欄位
   *
   * @param mixed bookId 若有傳入為修改，否則為新增
   * @param mixed format quick快速欄位｜normal完整欄位
   */
  public function form($params=[])
  {
    $meetingrooms = [];
    \Model\Orm\Meetingroom::all()->filter(function ($row) use (&$meetingrooms) {
      $meetingrooms[$row->id] = $row->label;
    });

    // @todo 取得登入資訊 配置login畫面，送出直接導向session

    // @todo 登出回到login

    $data = [
      'Meetingrooms' => $meetingrooms,
      'MeetingroomId' => intval($this->input->get('MeetingroomId'))
    ];
    $view['content']=$this->load->view('book_form', $data, TRUE);
    $this->load->view('layout', $view);
  }


  /**
   * === 處理 ===
   * 功能群組註解詳細說明
   */
  public function save()
  {
    if ($this->input->method() != "post") {
      // exception
      dd('禁止存取!');
    }
    // dd($this->input->post());

    // form-data valid
    $BookId = filter_var($this->input->post('id'), FILTER_VALIDATE_INT);

    // save relation data
    if ($BookId) {
      $book = \Model\Orm\BookRecord::find($BookId);
    } else {
      $book = new \Model\Orm\BookRecord();
    }

    try{
      $book->AccountId = filter_var($this->input->post('AccountId'), FILTER_VALIDATE_INT);
      $book->MeetingroomId = filter_var($this->input->post('MeetingroomId'), FILTER_VALIDATE_INT);

      // $book->startDateTime = strtotime($this->input->post('startDateTime'));
      // $book->endDateTime = strtotime($this->input->post('endDateTime'));
      $book->startDateTime = $this->input->post('startDateTime');
      $book->endDateTime = $this->input->post('endDateTime');

      $book->usage = strtolower(filter_var($this->input->post('usage'), FILTER_SANITIZE_STRING));
      $book->members = filter_var($this->input->post('members'), FILTER_VALIDATE_INT);
      $book->notice = filter_var($this->input->post('notice'), FILTER_SANITIZE_STRING);

      // @todo notify審核通知保留未實作
      $book->save();
      $this->session->set_flashdata('msg', '已完成會議室借用申請');
      redirect('/book');

    } catch (\Illuminate\Database\QueryException $e) {
      $error_code = $e->errorInfo[1];
      $resp = [
        'code' => $error_code,
        'errMsg' => '資料庫錯誤！' . $e->getMessage()
      ];
      $this->session->set_flashdata('errMsg', $resp['errMsg']);
      $this->session->set_flashdata('errCode', $resp['code']);
      redirect('/book/form');
    }

  }


  /**
   * === 借用紀錄列表 ===
   * dataTables UI
   */
  public function list($params = [])
  {
    $view['content'] = $this->load->view('book_list', NULL, TRUE);
    $this->load->view('layout', $view);
  }


  /**
   * === 處理查詢後端請求回應 ===
   * 1. 支援模糊搜尋：日期、文字格式判斷
   */
  public function ajaxSearch()
  {
    // get request and chech by php origional function

    //

    // response format
  }
}
