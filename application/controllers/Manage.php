<?php

use Illuminate\Database\Eloquent\Model;

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * === 系統管理 ===
 * @author San.Huang<san.huang@staff.nueip.com>
 *
 * 1. 使用者權限設定：列出使用者清單，可指定誰可成為借用者
 * 2. 偏好設定：配置預設借用用途以及規則（例如：直撥不能打擾、上課需要與會者完成報到登錄）
 */
class Manage extends CI_Controller
{

  public function index()
  {
    redirect('http://' . $_SERVER['HTTP_HOST'] . '/manage/usersList');
    // $view['content'] = $this->load->view('manage_index', NULL, TRUE);
    // $this->load->view('layout', $view);
  }


  /**
   * === 配置模擬登入畫面 ===
   * 1. 送出不做後端檢查直接指定管理者
   * 2. 直接判斷是否ajax-post
   */
  public function login()
  {
    //進行帳號驗證
    if( $this->input->method() == "post" ){
      // filter for normal string
      $account = filter_var( $this->input->post('account') );
      $password = filter_var($this->input->post('password'));

      if( empty($account) ){
        $resp = [
          // custom error code
          'code' => 5001,
          'errMsg' => '帳號未填寫！',
        ];
        // return $resp;
      }elseif( empty($password) ){
        $resp = [
          // custom error code
          'code' => 5002,
          'errMsg' => '帳號未填寫！',
        ];
        // return $resp;
      }else{
          $user_account = \Model\Orm\AccountInfo::where('account', $account)->first();

          if( !$user_account  ){
            $resp = [
              // custom error code
              'code' => 5003,
              'errMsg' => '找不到帳號！',
            ];
          }else{
            $this->load->library('encryption');
            $this->encryption->initialize(
              array(
                'driver' => 'openssl',
                'cipher' => 'blowfish',
                'mode' => 'cbc',
                'key' => $this->config->item('encryption_key'),
                )
              );
            if($password !== $this->encryption->decrypt($user_account->role->password) ){
              $resp = [
                // custom error code
                'code' => 5004,
                'errMsg' => '密碼輸入錯誤！',
              ];
              // return $resp;
            }else{
              $sData = $user_account->toArray();
              $role = $user_account->role->toArray();
              $sData['role'] = $role;
              // keep session
              $this->session->set_userdata('user_account', $sData);

            redirect('http://' . $_SERVER['HTTP_HOST'] . '/book/index');
            }
          }

        // dd($user_account->role->password);
        $this->session->set_flashdata('errMsg', $resp['errMsg']);
        $this->session->set_flashdata('errCode', $resp['code']);
      }

    }

    $this->load->view('login');
  }


  /**
   * === 登出功能 ===
   * 1. 清除session
   * 2. 導回登入
   */
  public function logout()
  {
    $this->session->unset_userdata('user_account');
    redirect('http://' . $_SERVER['HTTP_HOST'] . '/manage/login');
  }


  /**
   * === 使用者權限設定 ===
   * 1. dataTable列表呈現
   * 2. 直接畫面列附加下拉選擇角色權限
   */
  public function usersList($params=[])
  {
    // 取出account_info紀錄，預設(若沒配置account_role)全部帳號都是與會者
    $tabledatas = \Model\Orm\AccountInfo::all();

    $view['content']=$this->load->view('manage_users', ['tabledatas' => $tabledatas], TRUE);
    $this->load->view('layout', $view);
  }


  /**
   * Undocumented function
   *
   * @return void
   */
  public function usersForm()
  {
    // 部門
    $depts = \Model\Orm\DeptInfo::all();

    // 性別
    $sex = ['女','男'];

    // 身份
    $roles = ['與會者','借用者'];

    $data = [
      'modal_title' => $this->input->get('title'),
      'modal_action' => '/manage/usersSave',
      'depts' => $depts->toArray(),
      'roles' => $roles,
      'sex' => $sex
    ];

    if( ($pid=intval($this->input->get('primaryId'))) ){
      $account = \Model\Orm\AccountInfo::where('id', $pid)->first();
      if(!$account){
        $resp = [
          'code' => 5005,
          'errMsg' => '查無此帳號！'
        ];
        $this->session->set_flashdata('errMsg', $resp['errMsg']);
        $this->session->set_flashdata('errCode', $resp['code']);
      }else{
        $data['formdata'] = $account->toArray();
        $data['formdata']['role'] = $account->role->toArray();
      }
    }

    $this->load->view('manage_users_form_modal', $data);
  }


  /**
   * === 處理後端使用者權限儲存 ===
   * 1. 先完成post-redirect mode
   */
  public function usersSave()
  {
    if ($this->input->method() != "post") {
      dd('禁止存取!');
    }

    // form-data valid
    $account_id = filter_var($this->input->post('id'), FILTER_VALIDATE_INT);

    // save relation data
    if($account_id){
      $account = \Model\Orm\AccountInfo::find($account_id);
    }else{
      $account = new \Model\Orm\AccountInfo();
    }
    // 帳號：字母全部轉小寫才寫入資料庫
    $account->account = strtolower(filter_var($this->input->post('account'), FILTER_SANITIZE_STRING));
    $account->name = filter_var($this->input->post('name'), FILTER_SANITIZE_STRING);
    // 性別：顯示時為 男/女 ，資料庫儲存為 1/0
    $account->sex = filter_var($this->input->post('sex'), FILTER_VALIDATE_BOOLEAN);
    if (!DateTime::createFromFormat('Y-m-d', $this->input->post('brithday'))){
      $resp = [
        'code' => 5006,
        'errMsg' => '生日欄位格式錯誤！'
      ];
      $this->session->set_flashdata('errMsg', $resp['errMsg']);
      $this->session->set_flashdata('errCode', $resp['code']);
      redirect('http://' . $_SERVER['HTTP_HOST'] . '/manage/usersList');
    }else{
      $account->brithday = $this->input->post('brithday');
    }
    $account->email = filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL);
    $account->comment = filter_var($this->input->post('comment'), FILTER_SANITIZE_STRING);
    // $account->DeptId = filter_var($this->input->post('Dept'), FILTER_VALIDATE_INT);
    $account->save();

    if( $account_id ){
      $account->role->role = filter_var($this->input->post('role'), FILTER_SANITIZE_STRING);
    }else{
      $account->role = new \Model\Orm\AccountRole();
      $this->load->library('encryption');
      //rule: password equal to account
      $this->encryption->initialize(
        array(
          'driver' => 'openssl',
          'cipher' => 'blowfish',
          'mode' => 'cbc',
          'key' => $this->config->item('encryption_key'),
          )
        );
        $account->role->password = $this->encryption->encrypt($account->account);
    }
    // set relation of account_department
    $account->role->DeptId = filter_var($this->input->post('Dept'), FILTER_VALIDATE_INT);
    $account->role->role = filter_var($this->input->post('role'), FILTER_SANITIZE_STRING);

    $account->role()->save($account->role);

    $this->session->set_flashdata('msg', '已完成儲存使用者資料');
    redirect('http://'. $_SERVER['HTTP_HOST']. '/manage/usersList');
  }

  /**
   * === 處理使用者帳號刪除機制 ===
   * 1. 傳入pid查詢該使用者紀錄後直接刪除！
   * 2. 相關的借用與操作記錄則對應直接刪除
   */
  public function usersDelete()
  {
    if ($this->input->method() != "post") {
      dd('禁止存取!');
    }

    $account_id = filter_var($this->input->post('primaryId'), FILTER_VALIDATE_INT);
    if( !$account_id ){
      $resp = [
        'code' => 5007,
        'errMsg' => '資訊錯誤，找不到使用者！'
      ];
      echo json_encode($resp, JSON_UNESCAPED_UNICODE);
      // $this->session->set_flashdata('errMsg', $resp['errMsg']);
      // $this->session->set_flashdata('errCode', $resp['code']);
      // redirect('http://127.0.0.1:8080/manage/usersList');
    }

    // 使用try catch 偵測資料庫操作錯誤做對應處理機制
    try {
      $account = \Model\Orm\AccountInfo::find($account_id);
      $account->delete();
      $resp = [
        'code' => 200,
        'msg' => '已刪除使用者資料'
      ];
      $this->session->set_flashdata('msg', '已刪除使用者資料');
      echo json_encode($resp, JSON_UNESCAPED_UNICODE);
      // redirect('http://127.0.0.1:8080/manage/usersList');
    } catch (\Illuminate\Database\QueryException $e) {
      $error_code = $e->errorInfo[1];
      $resp = [
        'code' => $error_code,
        'errMsg' => '資料庫錯誤！'.$e->getMessage()
      ];
      echo json_encode($resp, JSON_UNESCAPED_UNICODE);
      // $this->session->set_flashdata('errMsg', $resp['errMsg']);
      // $this->session->set_flashdata('errCode', $resp['code']);
      // redirect('http://127.0.0.1:8080/manage/usersList');
    }
  }


  /**
   * === 處理使用者帳號批次刪除機制 ===
   * 1. 傳入pid陣列查詢該使用者紀錄後直接刪除！
   */
  public function usersDeleteSelect()
  {
    if ($this->input->method() != "post") {
      dd('禁止存取!');
    }

    if( !is_array($this->input->post('pid')) && count($this->input->post('pid')) ){
      $resp = [
        'code' => 5007,
        'errMsg' => '傳遞參數錯誤，無法刪除使用者！'
      ];
      $this->session->set_flashdata('errMsg', $resp['errMsg']);
      $this->session->set_flashdata('errCode', $resp['code']);
      redirect('http://' . $_SERVER['HTTP_HOST'] . '/manage/usersList');
    }else{
      $account_ids = $this->input->post('pid');
    }

    // 使用try catch 偵測資料庫操作錯誤做對應處理機制
    try {
      \Model\Orm\AccountInfo::whereIn('id', $account_ids)->delete();
      $resp = [
        'code' => 200,
        'msg' => '已刪除使用者資料'
      ];
      $this->session->set_flashdata('msg', '已刪除使用者資料');
      redirect('http://' . $_SERVER['HTTP_HOST'] . '/manage/usersList');
    } catch (\Illuminate\Database\QueryException $e) {
      $error_code = $e->errorInfo[1];
      $resp = [
        'code' => $error_code,
        'errMsg' => '資料庫錯誤！'.$e->getMessage()
      ];
      $this->session->set_flashdata('errMsg', $resp['errMsg']);
      $this->session->set_flashdata('errCode', $resp['code']);
      redirect('http://' . $_SERVER['HTTP_HOST'] . '/manage/usersList');
    }
  }




  /**
   * === 匯入使用者資訊 ===
   * 1. 根據定義欄位轉成csv檔案配置預設會議室清單進行匯入
   * 2. 提供檔案選擇拖曳上傳後機制
   */
  public function selectfile()
  {
  }


  /**
   * === 後端處理匯入會議室資訊 ===
   * 1. 檢查上傳的csv欄位對應
   * 2. 轉成資料庫欄位結構後逐筆寫入
   */
  public function import()
  {
    $config['upload_path'] = realpath(APPPATH . '../tmp/upload/');
		$config['allowed_types'] = 'csv|xls';
		$config['max_size']	= '100';
    $this->load->library('upload', $config);

    if ( !$this->upload->do_upload('file')) {
      $resp = [
        'code' => 5008,
        'errMsg' => $this->upload->display_errors()
      ];
      // 輸出錯誤訊息
      dd($this->upload->display_errors());
      $this->session->set_flashdata('errMsg', $resp['errMsg']);
      $this->session->set_flashdata('errCode', $resp['code']);
		} else {
      $file_path = $this->upload->data()['full_path'];

      /**
       * === csv process using third-party library ===
       * @see https://gist.github.com/rakibroni/38e89933577f5500b4fe
       */
      $this->load->library('csvimporter');
      if ($this->csvimporter->get_array($file_path)) {
        $csv_array = $this->csvimporter->get_array($file_path);
        // dd($csv_array);

        try {
          /**
           * === csv data structure ===
           * array: [
           * 0 => array:8 [
           *    "id" => "14"
           *    "account" => "demouser509"
           *    "name" => "John Doe"
           *    "brithday" => "2013-04-01"
           *    "email" => "doe.john@doe.org"
           *    "comment" => "NULL"
           *    "sex" => "0"
           *    "DeptId" => "NULL"
           *  ]
           * ]
           */
          foreach ($csv_array as $row) {
            // db process
            if (!empty($row['id'])) {
              $raw = \Model\Orm\AccountInfo::find($row['id']);
              if (!$raw) {
                $raw = new \Model\Orm\AccountInfo();
              }
            } elseif (!empty($row['account'])) {
              $raw = \Model\Orm\AccountInfo::where('account', $row['account'])->first();
              if (!$raw) {
                $raw = new \Model\Orm\AccountInfo();
              }
            } else {
              $raw = new \Model\Orm\AccountInfo();
            }

            // remove primary key
            unset($row['id']);
            // fill fields
            $raw->fill($row);
            // record update
            $raw->push();
          }

          $resp = [
            'code' => 200,
            'msg' => '已成功匯入'.count($csv_array).'筆資料!'
          ];
          echo json_encode($resp, JSON_UNESCAPED_UNICODE);

        } catch (\Illuminate\Database\QueryException $e) {
          $error_code = $e->errorInfo[1];
          $resp = [
            'code' => $error_code,
            'errMsg' => '資料庫錯誤！' . $e->getMessage()
          ];
          $this->session->set_flashdata('errMsg', $resp['errMsg']);
          $this->session->set_flashdata('errCode', $resp['code']);
          // redirect('http://127.0.0.1:8080/manage/usersList');
        }
      }
		}

  }


  /**
   * === 使用者帳號匯出並下載  ===
   * 1. codeigniter3 download helper for simple way export
   * 2. use nueip/io process export xls(need to redefine fileds)
   */
  public function usersExport()
  {
    // $this->load->helper('download');
    // $data = 'Here is some text!';
    // $name = 'accountinfo_export.csv';
    // force_download($name, $data);

    // IO物件建構
    $io = new \nueip\io\IO();
    // 匯出處理 - 建構匯出資料 - 簡易模式結構定義物件-範本
    $datas = \Model\Orm\AccountInfo::all();
    $io->export($datas->toArray(), $config = 'SimpleExample', $builder = 'Excel', $style = 'Io');

  }


  /**
   * === 偏好設定 ===
   * 1. 固定表單欄位：用途對應提示規則(固定)
   * 2. 借用公告資訊：提示規則 1.單日借 2.連續時間 3.特定時段不能借
   */
  public function settings($params = [])
  {
    $view['content'] = $this->load->view('manage_settings', NULL, TRUE);
    $this->load->view('layout', $view);
  }


  /**
   * === 處理後端偏好設定儲存 ===
   * 1.
   */
  public function saveSettings()
  {
  }
}
