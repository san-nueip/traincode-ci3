<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
/**
 * === 載入ORM class使用方式 ===
 * @author San.Huang<san.huang@staff.nueip.com>
 *
 * 	use Model\Orm\AccountInfo;
 * 	use Model\Orm\AccountRole;
 * 	use Model\Orm\DeptInfo;
 * 	use Model\Orm\BookRecord;
 * 	use Model\Orm\BookStatecode;
 * 	use Model\Orm\Meetingroom;
 * 	use Model\Orm\MeetingroomUsage;
 *	use Model\Orm\Settings;
 */
class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}


	/**
	 * === [資料庫密碼]加解密測試函式 ===
	 * 1. read key confige from config/config.php :: encryption_key
	 * 2. encrypt string paramester
	 * 3. check decrypter 2. string to compare with origin
	 * 4. store encrypt string in account_info
	 */
	public function encrypter()
	{
		$key = $this->config->item('encryption_key');
		$this->load->library('encryption');

		$this->encryption->initialize(
			array(
				'driver' => 'openssl',
				'cipher' => 'blowfish',
				'mode' => 'cbc',
				'key' => $key,
			)
		);
		$plain_text = 'demo123';
		echo "\$plain_text:{$plain_text}";
		echo " <BR/>";
		echo " <BR/>";
		$cipher_text = $this->encryption->encrypt($plain_text);
		echo "\$cipher_text:{$cipher_text}";
		echo " <BR/>";
		echo "\$cipher_text length:".strlen($cipher_text);
		echo " <BR/>";
		echo " <BR/>";
		echo "decrypt: ".$this->encryption->decrypt($cipher_text);
		// dd($key);

	}
}
