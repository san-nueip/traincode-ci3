<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * === 會議室管理 ===
 * @author San.Huang<san.huang@staff.nueip.com>
 *
 * 1. dataTable 匯入功能(拖曳上傳匯入)
 * 2.
 */
class Meetingroom extends CI_Controller
{

  /**
   * === 會議室列表資訊 ===
   * 1. 以dataTable顯示列表
   * 2. 新增匯入按鈕
   * 3. 會議室
   */
  public function index()
  {
    // load meetingroom from orm
    $tabledatas = Model\Orm\Meetingroom::all();
    // dd($view['tabledatas']->toArray());

    $view['content'] = $this->load->view('meetingroom_index', ['tabledatas'=> $tabledatas->toArray()], TRUE);
    $this->load->view('layout', $view);
  }


  /**
   * === 後端處理匯出會議室資訊 ===
   * 1. 檢查上傳的csv欄位對應
   * 2. 轉成資料庫欄位結構後逐筆寫入
   */
  public function export()
  {
  }


  /**
   * === modal顯示會議室資訊 ===
   * 1. 將view轉乘以html回應格式顯示dialog內容變數回應前端ajax
   * 2. 判斷是否有主鍵變數來決定新增還是修改
   */
  public function form()
  {
    $data = [
      'modal_title' => '新增會議室',
      'modal_action'=> '/meetingroom/save'
    ];
    $this->load->view('meetingroom_form_modal', $data);
  }


  /**
   * === 處理儲存會議室 ===
   */
  public function save()
  {
    if( $this->input->method() != "post" ){
      // exception
    }
    dd($this->input->post());
  }


  /**
   * === soft-delete機制移除(下架)會議室 ===
   * 1. 以設定狀態欄位方式讓會議室不出現在使用者可選列表
   * 2. 以設定狀態欄位方式讓會議室移除在會議室列表
   */
  public function delete()
  {

  }
}
