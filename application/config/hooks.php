<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/

/**
 * @see https://stackoverflow.com/questions/8674166/access-the-ci-session-in-a-pre-controller-codeigniter-hook
 *
 * 狀況：
 * 原本使用pre_controller，但會持續出現
 * 1. Unable to locate the specified class: Session.php in Codeigniter
 * 2. session already start之類的問題
 *
 * 解法：
 * 使用post_controller
 */
$hook['post_controller'] = array(
                            'class'    => 'AuthCheck',
                            'function' => 'ifLogin',
                            'filename' => 'AuthCheck.php',
                            'filepath' => 'hooks',
                            'params'   => array()
                          );
