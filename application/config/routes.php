<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// 配置動態路由 URI /code/[Action] 直接對應controllers/Code.php 相同Action method
$route['code/(:any)'] = 'code/$1';
// $route['models/(:any)/(:int)'] = 'cimodel/$1/$2';
$route['models/(:any)'] = 'cimodel/$1';
// 配置實作借用操作流程
$route['book/(:any)'] = 'book/$1';
$route['board/(:any)'] = 'board/$1';
$route['meetingroom/(:any)'] = 'meetingroom/$1';
$route['manage/(:any)'] = 'manage/$1';

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
