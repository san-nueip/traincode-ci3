<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as Capsule;

$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'mysql:host=127.0.0.1;dbname=codedemo',
	'username' => 'san',
	'password' => 'omge0954',
	'database' => 'codedemo',
	'dbdriver' => 'pdo',
	'dbprefix' => '',
	'pconnect' => TRUE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

$capsule = new Capsule;
$capsule->addConnection([
	// @todo 確認為何不支援pdo？？
	'driver'    => 'mysql',
	'host'      => '127.0.0.1',
	'database'  => 'codedemo',
	'username'  => $db['default']['username'],
	'password'  => $db['default']['password'],
	'charset'   => $db['default']['char_set'],
	'collation' => $db['default']['dbcollat'],
	'prefix'    => $db['default']['dbprefix'],
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();