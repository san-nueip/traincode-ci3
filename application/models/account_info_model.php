<?php

/**
 * 帳號資訊表
 *
 * @author San.Huang<san.huang@staff.nueip.com>
 */
class account_info_model extends CI_Model
{
  /**
   * 資料表名稱
   */
  protected $table = "account_info";

  /**
   * 欄位資料
   */
  protected $tableColumns = [
    'id',
    'account',
    'name',
    'brithday',
    'email',
    'comment',
  ];


  public function __construct()
  {
    parent::__construct();
    // 載入資料連線
    $this->load->database();
  }

}