<?php

/**
 * 會議室佔用時間表
 *
 * @author San.Huang<san.huang@staff.nueip.com>
 */
class meetingroom_usage_model extends CI_Model
{
  /**
   * 資料表名稱
   */
  protected $table = "meetingroom_usage";

  /**
   * 欄位資料
   */
  protected $tableColumns = [
    'id',
    'MeetingroomId',
    'BookId',
    'bookStart',
    'bookEnd',
  ];


  public function __construct()
  {
    parent::__construct();
    // 載入資料連線
    $this->load->database();
  }

}