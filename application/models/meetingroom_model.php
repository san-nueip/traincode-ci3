<?php

/**
 * 會議室資訊表
 *
 * @author San.Huang<san.huang@staff.nueip.com>
 */
class meetingroom_model extends CI_Model
{
  /**
   * 資料表名稱
   */
  protected $table = "meetingroom";

  /**
   * 欄位資料
   */
  protected $tableColumns = [
    'id',
    'label',
    'location',
    'limit',
    'bookRule',
    'defaultUsage',
  ];


  public function __construct()
  {
    parent::__construct();
    // 載入資料連線
    $this->load->database();
  }

}