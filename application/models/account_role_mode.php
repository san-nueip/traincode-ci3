<?php

/**
 * 帳號角色表
 *
 * @author San.Huang<san.huang@staff.nueip.com>
 */
class account_role_model extends CI_Model
{
  /**
   * 資料表名稱
   */
  protected $table = "account_role";

  /**
   * 欄位資料
   */
  protected $tableColumns = [
    'id',
    'AccountId',
    'role',
  ];


  public function __construct()
  {
    parent::__construct();
    // 載入資料連線
    $this->load->database();
  }

}