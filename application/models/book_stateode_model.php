<?php

/**
 * 借用紀錄狀態表
 *
 * @author San.Huang<san.huang@staff.nueip.com>
 */
class book_statecode_model extends CI_Model
{
  /**
   * 資料表名稱
   */
  protected $table = "book_statecode";

  /**
   * 欄位資料
   */
  protected $tableColumns = [
    'id',
    'BookId',
    'AccountId',
    'statecode',
    'setTime',
    'comment',
  ];


  public function __construct()
  {
    parent::__construct();
    // 載入資料連線
    $this->load->database();
  }

}