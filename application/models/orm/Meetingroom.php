<?php

namespace Model\Orm;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Meetingroom extends Eloquent
{
  protected $table = 'meetingroom';

  public $timestamps = false;

  /* setting field mapping */
}
