<?php

namespace Model\Orm;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class DeptInfo extends Eloquent
{
  protected $table = 'dept_info';

  public $timestamps = false;
}
