<?php

namespace Model\Orm;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class AccountRole extends Eloquent
{
  protected $table = 'account_role';

  public $timestamps = false;

  public function account()
  {
    // define relationship (ormClassName, foreign_key, local_key)
    return $this->belongsTo('Model\Orm\AccountInfo', 'AccountId', 'id');
  }
}
