<?php

namespace Model\Orm;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Settings extends Eloquent
{
  protected $table = 'settings';

  public $timestamps = false;
}
