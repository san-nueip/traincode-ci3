<?php

namespace Model\Orm;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class BookRecord extends Eloquent
{
  protected $table = 'book_record';

  public $timestamps = false;

  public function AccountInfo()
  {
    return $this->belongsTo('Model\Orm\AccountInfo', 'AccountId', 'id');
  }

  public function Meetingroom()
  {
    return $this->belongsTo('Model\Orm\Meetingroom', 'MeetingroomId', 'id');
  }
}
