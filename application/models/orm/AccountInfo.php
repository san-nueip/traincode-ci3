<?php

namespace Model\Orm;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class AccountInfo extends Eloquent
{
  protected $table = 'account_info';

  public $timestamps = false;

  /**
   * === 使用fill-push進行匯入資料 ===
   * 針對批次匯入資料進行已存在紀錄做內容替換取代
   * @see https://stackoverflow.com/questions/24109535/how-to-update-column-value-in-laravel
   * 必須建立$fillable清單
   * @see https://stackoverflow.com/questions/53793841/add-title-to-fillable-property-to-allow-mass-assignment-on-app-post
   *
   * Usage:
   * ormObject->fill($dataArray[]); (without primary key!!)
   * ormObject->push();
   */
  protected $fillable = [
    "account",
    "name",
    "brithday",
    "email",
    "comment",
    "sex",
    "DeptId",
  ];

  public function role()
  {
    // define 1-1 relationship(ormClassName, 'foreign_key', 'local_key');
    return $this->hasOne('Model\Orm\AccountRole', 'AccountId', 'id');
  }

  public function records()
  {
    // define 1-1 relationship(ormClassName, 'foreign_key', 'local_key');
    return $this->hasMany('Model\Orm\BookRecord', 'AccountId', 'id');
  }


}
