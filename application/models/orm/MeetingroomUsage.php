<?php

namespace Model\Orm;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class MeetingroomUsage extends Eloquent
{
  protected $table = 'meetingroom_usage';

  public $timestamps = false;
}
