<?php

namespace Model\Orm;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class BookStatecode extends Eloquent
{
  protected $table = 'book_statecode';

  public $timestamps = false;
}
