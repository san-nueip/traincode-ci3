<?php

/**
 * 借用紀錄表
 *
 * @author San.Huang<san.huang@staff.nueip.com>
 */
class book_record_model extends CI_Model
{
  /**
   * 資料表名稱
   */
  protected $table = "book_record";

  /**
   * 欄位資料
   */
  protected $tableColumns = [
    'id',
    'AccountId',
    'MeetingroomId',
    'usage',
    'members',
    'notice',
    'startDateTime',
    'endDateTime',
  ];


  public function __construct()
  {
    parent::__construct();
    // 載入資料連線
    $this->load->database();
  }

}