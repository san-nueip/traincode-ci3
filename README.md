# 建立步驟

參考[php – Codeigniter – 如何讓 System 交給 Composer 保管 | jsnWork](http://jsnwork.kiiuo.com/archives/2869/php-codeigniter-%e5%a6%82%e4%bd%95%e8%ae%93-system-%e4%ba%a4%e7%b5%a6-composer-%e4%bf%9d%e7%ae%a1/)配置，使用ci3.1.11版本

## composer建立專案vendor目錄
```bash
composer init
composer require codeigniter/framework 3.1.9
```

## 複製vendor下檔案
將這兩個檔案複製到你的網站根目錄
```
vendor/codeigniter/framework/application/
vendor/codeigniter/framework/index.php
```

## 修改 index.php
```php
// 找到$system_path改成
$system_path = 'vendor/codeigniter/framework/system';
```

### 如何升級/降級 Codeigniter？
直接修改 composer.json 中的版本，例如我把 3.1.9 降級為 3.1.8
```json
"require": {
    "codeigniter/framework": "3.1.8"
}
```
```bash
composer update
```