# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.29)
# Database: codedemo
# Generation Time: 2020-04-16 10:08:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table account_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_info`;

CREATE TABLE `account_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(30) NOT NULL DEFAULT '' COMMENT '帳號',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '姓名',
  `brithday` date DEFAULT NULL COMMENT '生日',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '信箱',
  `comment` text COMMENT '備註',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='帳號資訊表';

LOCK TABLES `account_info` WRITE;
/*!40000 ALTER TABLE `account_info` DISABLE KEYS */;

INSERT INTO `account_info` (`id`, `account`, `name`, `brithday`, `email`, `comment`)
VALUES
	(2,'p0065','San.Huang','1983-04-05','personalwork@me.com','insert by inert()'),
	(4,'user1','SanHuang','1983-04-05','san.huang@staff.nueip.com','insert by pure sql');

/*!40000 ALTER TABLE `account_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_role`;

CREATE TABLE `account_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `AccountId` int(11) NOT NULL COMMENT '關聯帳號',
  `role` varchar(10) DEFAULT NULL COMMENT '角色',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='帳號角色表';



# Dump of table book_record
# ------------------------------------------------------------

DROP TABLE IF EXISTS `book_record`;

CREATE TABLE `book_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `AccountId` int(11) DEFAULT NULL,
  `MeetingroomId` int(11) DEFAULT NULL,
  `usage` varchar(10) DEFAULT NULL COMMENT '用途',
  `members` tinyint(3) DEFAULT NULL COMMENT '與會人數',
  `notice` text COMMENT '注意事項',
  `startDateTime` datetime DEFAULT NULL COMMENT '會議借用開始時間',
  `endDateTime` datetime DEFAULT NULL COMMENT '會議借用結束時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='借用紀錄表';



# Dump of table book_stateode
# ------------------------------------------------------------

DROP TABLE IF EXISTS `book_stateode`;

CREATE TABLE `book_stateode` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `BookId` int(11) NOT NULL COMMENT '關聯借用紀錄編號',
  `AccountId` int(11) DEFAULT NULL COMMENT '關聯帳號',
  `statecode` tinyint(1) NOT NULL COMMENT '狀態識別碼',
  `setTime` int(11) NOT NULL COMMENT '設定狀態紀錄時間',
  `comment` varchar(255) DEFAULT NULL COMMENT '備註資訊',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='借用紀錄狀態表';



# Dump of table dept_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dept_info`;

CREATE TABLE `dept_info` (
  `d_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '系統序號',
  `d_code` varchar(64) DEFAULT NULL COMMENT '識別碼',
  `d_name` varchar(64) NOT NULL COMMENT '名稱',
  `d_level` varchar(64) NOT NULL COMMENT '層級',
  `date_start` date NOT NULL COMMENT '開始日',
  `date_end` date NOT NULL COMMENT '結束日',
  `remark` text NOT NULL COMMENT '備註',
  `date_create` datetime NOT NULL COMMENT '建立日',
  `user_create` int(10) NOT NULL COMMENT '建立者',
  `date_update` datetime NOT NULL COMMENT '更新日',
  `user_update` int(10) NOT NULL COMMENT '更新者',
  `date_delete` datetime NOT NULL COMMENT '軟刪日',
  `user_delete` int(10) NOT NULL COMMENT '軟刪者',
  `rec_status` tinyint(1) NOT NULL COMMENT '資料狀態',
  PRIMARY KEY (`d_id`),
  KEY `d_code` (`d_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='單位資料';

LOCK TABLES `dept_info` WRITE;
/*!40000 ALTER TABLE `dept_info` DISABLE KEYS */;

INSERT INTO `dept_info` (`d_id`, `d_code`, `d_name`, `d_level`, `date_start`, `date_end`, `remark`, `date_create`, `user_create`, `date_update`, `user_update`, `date_delete`, `user_delete`, `rec_status`)
VALUES
	(1,'d0004','部門1305','組','2020-01-01','0000-00-00','部門4','2020-04-14 06:58:20',0,'2020-04-14 06:58:20',0,'2020-04-14 06:58:20',0,0),
	(2,'d0001','部門3100','組','2020-01-01','0000-00-00','部門1','2020-04-16 07:42:59',0,'2020-04-16 07:42:59',0,'2020-04-16 07:42:59',0,0);

/*!40000 ALTER TABLE `dept_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table meetingroom
# ------------------------------------------------------------

DROP TABLE IF EXISTS `meetingroom`;

CREATE TABLE `meetingroom` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `label` varchar(10) NOT NULL DEFAULT '' COMMENT '空間名稱',
  `location` int(11) NOT NULL COMMENT '位置',
  `limit` smallint(3) NOT NULL COMMENT '上限人數',
  `bookRule` text COMMENT '借用說明',
  `defaultUsage` int(11) DEFAULT NULL COMMENT '預設用途資訊',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='會議室資訊表';



# Dump of table meetingroom_usage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `meetingroom_usage`;

CREATE TABLE `meetingroom_usage` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `MeetingroomId` tinyint(3) NOT NULL COMMENT '關聯會議室編號',
  `BookId` tinyint(3) NOT NULL COMMENT '關聯借用紀錄編號',
  `bookStart` datetime NOT NULL COMMENT '借用開始時間',
  `bookEnd` datetime NOT NULL COMMENT '借用結束時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='會議室佔用時間表';



# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `key` varchar(20) NOT NULL DEFAULT '' COMMENT '鍵值',
  `value` int(11) DEFAULT NULL COMMENT '參數',
  `enabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否啟用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系統設定';




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
